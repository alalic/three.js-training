import { Scene, PerspectiveCamera, WebGLRenderer, Vector3, LineBasicMaterial, Line, Geometry } from 'three';

//: Creating a scene
let scene = new Scene();
let camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 500);
camera.position.set(0, 0, 100);
camera.lookAt(0, 0, 0);

let renderer = new WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

let geometry = new Geometry();
geometry.vertices.push(new Vector3(-10, 0, 0,));
geometry.vertices.push(new Vector3(0, 10, 0,));
geometry.vertices.push(new Vector3(10, 0, 0,));
let material = new LineBasicMaterial({ color: 0xe6c0e9 });
let line = new Line(geometry, material);
scene.add(line);
renderer.render(scene, camera);