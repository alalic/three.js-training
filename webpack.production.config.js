import * as path from 'path';

import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';


export default {
    context: __dirname,

    entry: {
        index: path.resolve(__dirname, './src/index.js')
    },

    output: {
        filename: 'js/[name].bundle.js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },

    mode: 'production',

    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin(
            {
                filename: 'index.html',
                template: path.resolve(__dirname, './src/index.html')
            }
        )
    ]
};