import * as path from 'path';

import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';


export default {
    context: __dirname,

    entry: {
        index: path.resolve(__dirname, './src/index.js')
    },

    output: {
        filename: 'js/[name].bundle.js',
        path: path.resolve(__dirname, './dist'),
        crossOriginLoading: 'anonymous',
        publicPath: ''
    },

    mode: 'development',

    devServer: {
        contentBase: path.resolve(__dirname, './dist'),
        port: 9000,
        open: true
    },

    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin(
            {
                filename: 'index.html',
                template: path.resolve(__dirname, './src/index.html')
            }
        )
    ]
};